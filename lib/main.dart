import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:my_money_tracker/models/categories.dart';
import 'package:my_money_tracker/models/transactions.dart';
import 'package:my_money_tracker/utils/localstorage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_money_tracker/pages/categories.dart';
import 'package:my_money_tracker/pages/transactions.dart';
import 'package:my_money_tracker/pages/stats.dart';
import 'package:provider/provider.dart';

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

void main() async {
  flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
  runApp(MoneyTrackerApp());
}

class MoneyTrackerApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MoneyTrackerAppState createState() => _MoneyTrackerAppState();
}

class _MoneyTrackerAppState extends State<MoneyTrackerApp> {
  Future<bool> localStorageReady;
  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      home: MainPage(),
      localizationsDelegates: [
        DefaultMaterialLocalizations.delegate,
        DefaultCupertinoLocalizations.delegate,
        DefaultWidgetsLocalizations.delegate,
      ],
    );
  }
}

class MainPage extends StatefulWidget {
  MainPage({Key key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  Future<bool> _localStorageReadyFuture;

  @override
  void initState() {
    super.initState();

    // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('app_icon');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings);

    // Show a notification every minute with the first appearance happening a minute after invoking the method
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'daily_reminder',
        'Daily reminder',
        'Reminds users to enter their daily transactions',
        playSound: true);
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    flutterLocalNotificationsPlugin.periodicallyShow(
        0,
        'Daily transactions reminder',
        'Don\'t forget to add your daily transactions',
        RepeatInterval.Daily,
        platformChannelSpecifics);

    _localStorageReadyFuture = LocalStorageProvider.instance.initLocalStorage();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _localStorageReadyFuture,
      builder: (context, AsyncSnapshot<bool> snapshot) {
        if (snapshot.data == true) {
          return MultiProvider(
            providers: [
              ChangeNotifierProvider(builder: (context) {
                CategoryStore categoryStore = new CategoryStore();
                categoryStore.load();
                return categoryStore;
              }),
              ChangeNotifierProvider(builder: (context) {
                TransactionStore transactionStore = new TransactionStore();
                transactionStore.load();
                return transactionStore;
              })
            ],
            child: buildTabs(context, snapshot.data),
          );
        }
        return buildTabs(context, snapshot.data);
      },
    );
  }

  Widget buildTabs(BuildContext context, bool storageReady) {
    return CupertinoTabScaffold(
      tabBar: CupertinoTabBar(items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.home), title: Text('Transactions')),
        BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.folder), title: Text('Categories')),
        BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.eye), title: Text('Stats')),
      ]),
      tabBuilder: (BuildContext context, int index) {
        return CupertinoTabView(builder: (BuildContext context) {
          if (storageReady == true) {
            switch (index) {
              case 0:
                return TransactionsPage();
                break;
              case 1:
                return CategoriesPage();
                break;
              case 2:
                return StatsPage();
                break;
            }
          }
          return CupertinoActivityIndicator(
            animating: true,
            radius: 30,
          );
        });
      },
    );
  }
}
