import 'dart:core';

import 'package:flutter/material.dart';
import 'package:my_money_tracker/utils/localstorage.dart';

class Transaction {
  int _id;
  String _title;
  String _description;
  int _amount;
  DateTime _date;
  int _transactionCategory;

  int get id => _id;
  String get title => _title;
  String get description => _description;
  int get amount => _amount;
  DateTime get date => _date;
  int get transactionCategory => _transactionCategory;

  Transaction({id, title, description, amount, date, transactionCategory}) {
    _id = id;
    _title = title;
    _description = description;
    _amount = amount;
    _date = date;
    _transactionCategory = transactionCategory;
  }

  factory Transaction.fromEncoded(Map<String, dynamic> data) => new Transaction(
        id: data['id'],
        title: data['title'],
        description: data['description'],
        amount: data['amount'],
        date: DateTime.parse(data['date']),
        transactionCategory: data['transaction_category'],
      );

  Map<String, dynamic> encode() => {
        "id": id,
        "title": title,
        "description": description,
        "amount": amount,
        "date": date.toIso8601String(),
        "transaction_category": transactionCategory,
      };

  @override
  String toString() {
    return '$id, $title, $description, $transactionCategory';
  }
}

class TransactionList {
  List<Transaction> transactions;

  TransactionList() {
    transactions = new List();
  }

  TransactionList.fromEncoded(List encoded) {
    transactions = new List();

    encoded.forEach((item) {
      final transaction = new Transaction.fromEncoded(item);
      transactions.add(transaction);
    });
  }

  encode() {
    return transactions.map((item) {
      return item.encode();
    }).toList();
  }
}

class TransactionStore with ChangeNotifier {
  TransactionList _transactions;
  bool _isLoading;
  bool _isLoaded;
  Map<int, int> _indexedTransactions;
  Map<int, List<int>> _transactionsIdxByCategoryId;

  List<Transaction> get transactions => _transactions.transactions;
  Map<int, List<int>> get transactionIdxByCategoryId =>
      _transactionsIdxByCategoryId;
  get isLoading => _isLoading;
  get isLoaded => _isLoaded;

  TransactionStore() {
    _transactions = new TransactionList();
    _indexedTransactions = new Map();
    _transactionsIdxByCategoryId = new Map();
    _isLoading = false;
    _isLoaded = false;
  }

  void regenerateIndex() {
    _transactionsIdxByCategoryId.clear();
    for (int i = 0; i < transactions.length; ++i) {
      if (_transactionsIdxByCategoryId[transactions[i].transactionCategory] ==
          null) {
        _transactionsIdxByCategoryId[transactions[i].transactionCategory] =
            new List();
      }
      _transactionsIdxByCategoryId[transactions[i].transactionCategory].add(i);
    }

    _indexedTransactions.clear();
    for (int i = 0; i < transactions.length; ++i) {
      _indexedTransactions[transactions[i].id] = i;
    }
  }

  void load() async {
    _transactions.transactions.clear();
    var savedItems =
        LocalStorageProvider.instance.localStorage.getItem('transactions');
    print(savedItems);

    if (savedItems != null) {
      _transactions = TransactionList.fromEncoded(savedItems as List);
    }
    regenerateIndex();
    notifyListeners();
  }

  Future save() {
    print(_transactions.encode());
    regenerateIndex();
    return LocalStorageProvider.instance.localStorage
        .setItem('transactions', _transactions.encode());
  }

  void addTransaction(Transaction transaction) {
    transactions.add(transaction);
    transactions.sort((trans1, trans2) {
      return trans1.date.compareTo(trans2.date);
    });
    regenerateIndex();
    notifyListeners();
    save();
  }

  void updateTransaction(Transaction transaction) {
    // Find category idx by id
    int idx = _indexedTransactions[transaction.id];
    transactions[idx] = transaction;
    regenerateIndex();
    notifyListeners();
    save();
  }

  void recategorizeTransactions(int fromCategoryId, int toCategoryId) {
    // Find category idx by id
    for (int i = 0; i < transactions.length; ++i) {
      if (transactions[i].transactionCategory == fromCategoryId) {
        transactions[i]._transactionCategory = toCategoryId;
      }
    }
    regenerateIndex();
    notifyListeners();
    save();
  }

  void removeTransaction(int index) {
    transactions.removeWhere((Transaction trans) => trans.id == index);
    regenerateIndex();
    notifyListeners();
    save();
  }

  Transaction transactionById(int index) {
    try {
      return transactions.firstWhere((Transaction trans) => trans.id == index);
    } catch (e) {
      return null;
    }
  }

  List<Transaction> transactionsInCategory(int categoryId) {
    return transactions
        .where((Transaction trans) => trans.transactionCategory == categoryId)
        .toList();
  }
}
