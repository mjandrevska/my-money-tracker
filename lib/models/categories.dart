import 'dart:core';

import 'package:flutter/material.dart';
import 'package:my_money_tracker/utils/localstorage.dart';

const List<String> transactionTypes = <String>['Income', 'Expense'];

class Category {
  int _id;
  String _title;
  String _description;
  int _transactionType;
  Color _color;

  int get id => _id;
  String get title => _title;
  String get description => _description;
  int get transactionType => _transactionType;
  Color get color => _color;

  Category({id, title, description, transactionType, color}) {
    _id = id;
    _title = title;
    _description = description;
    _transactionType = transactionType;
    _color = color;
  }

  factory Category.fromEncoded(Map<String, dynamic> data) => new Category(
        id: data['id'],
        title: data['title'],
        description: data['description'],
        transactionType: data['transaction_type'],
        color: new Color(data['color']),
      );

  Map<String, dynamic> encode() => {
        "id": id,
        "title": title,
        "description": description,
        "transaction_type": transactionType,
        "color": color.value,
      };

  @override
  String toString() {
    return '$id, $title, $description, $transactionType';
  }
}

class CategoryList {
  List<Category> categories;

  CategoryList() {
    categories = new List();
  }

  CategoryList.fromEncoded(List encoded) {
    categories = new List();

    encoded.forEach((item) {
      final category = new Category.fromEncoded(item);
      categories.add(category);
    });
  }

  encode() {
    return categories.map((item) {
      return item.encode();
    }).toList();
  }
}

class CategoryStore with ChangeNotifier {
  CategoryList _categories;
  bool _isLoading;
  bool _isLoaded;

  Map<int, int> _indexedCategories;

  List<Category> get categories => _categories.categories;
  Map<int, int> get categoriesIdxById => _indexedCategories;
  get isLoading => _isLoading;
  get isLoaded => _isLoaded;

  CategoryStore() {
    _categories = new CategoryList();
    _indexedCategories = new Map();
    _isLoading = false;
    _isLoaded = false;
  }

  void regenerateIndex() {
    _indexedCategories.clear();
    for(int i=0; i<categories.length; ++i) {
      _indexedCategories[categories[i].id] = i;
    }
  }

  void load() async {
    _categories.categories.clear();
    var savedItems =
        LocalStorageProvider.instance.localStorage.getItem('categories');

    if (savedItems != null) {
      _categories = CategoryList.fromEncoded(savedItems as List);
    }
    regenerateIndex();
    notifyListeners();
  }

  Future save() {
    print(_categories.encode());
    return LocalStorageProvider.instance.localStorage
        .setItem('categories', _categories.encode());
  }

  void addCategory(Category categoryData) {
    categories.add(categoryData);
    regenerateIndex();
    notifyListeners();
    save();
  }

  void updateCategory(Category categoryData) {
    // Find category idx by id
    int idx = _indexedCategories[categoryData.id];
    categories[idx] = categoryData;
    regenerateIndex();
    notifyListeners();
    save();
  }

  void removeCategory(int id) {
    categories.removeWhere((Category cat) => cat.id == id);
    regenerateIndex();
    notifyListeners();
    save();
  }

  List<Category> categoryByType(int type) {
    return categories.where((cat) => cat._transactionType == type).toList();
  }

  Category categoryById(int index) {
    try {
      return categories[categoriesIdxById[index]];
    } catch (e) {
      return null;
    }
  }
}
