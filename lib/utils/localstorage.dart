import 'package:localstorage/localstorage.dart';

class LocalStorageProvider {
  LocalStorageProvider._init();
  static final LocalStorageProvider _instance = new LocalStorageProvider._init();
  static LocalStorageProvider get instance => _instance;

  LocalStorage localStorage;

  Future<bool> initLocalStorage() async {
    localStorage = new LocalStorage('money_tracker');
    return localStorage.ready;
  }
}