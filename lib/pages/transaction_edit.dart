import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_money_tracker/models/categories.dart';
import 'package:my_money_tracker/models/transactions.dart';
// import 'package:my_money_tracker/pages/ocr.dart';
import 'package:provider/provider.dart';

class TransactionEditPage extends StatefulWidget {
  final int transactionId;
  TransactionEditPage({Key key, this.transactionId}) : super(key: key);

  @override
  _TransactionEditPageState createState() => _TransactionEditPageState();
}

class _TransactionEditPageState extends State<TransactionEditPage> {
  final _formKey = GlobalKey<FormState>();

  bool _inited;
  int _transactionId;

  String _title;
  String _description;
  int _transactionCategory = 0;
  int _amount = 0;
  DateTime _transactionTime = DateTime.now();

  int _transactionType = 0;

  @override
  void initState() {
    super.initState();

    _transactionId = widget.transactionId;
    _inited = false;
  }

  void saveTransaction() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      Navigator.of(context).pop(false);
      TransactionStore transStore = Provider.of<TransactionStore>(context);

      if (_transactionId == null) {
        int id;
        do {
          id = Random.secure().nextInt(1 << 32);
        } while (transStore.transactionById(id) != null);

        transStore.addTransaction(Transaction(
            id: id,
            title: _title,
            description: _description,
            date: _transactionTime,
            amount: _transactionType == 1 ? -_amount : _amount,
            transactionCategory: _transactionCategory));
      } else {
        transStore.updateTransaction(Transaction(
            id: _transactionId,
            title: _title,
            description: _description,
            date: _transactionTime,
            amount: _transactionType == 1 ? -_amount : _amount,
            transactionCategory: _transactionCategory));
      }
    }
  }

  void setTransactionCategory(id) {
    setState(() {
      _transactionCategory = id;
    });
  }

  // Future scan() async {
  //   try {
  //     String barcode = await BarcodeScanner.scan();
  //     print(barcode);
  //     List<int> bytes = utf8.encode(barcode);
  //     for(int i=0; i<bytes.length; ++i) {
  //       print(bytes[i]);
  //     }
  //     // setState(() => this.barcode = barcode);
  //   } on PlatformException catch (e) {
  //     print('Exception?');
  //     // if (e.code == BarcodeScanner.CameraAccessDenied) {
  //     //   setState(() {
  //     //     this.barcode = 'The user did not grant the camera permission!';
  //     //   });
  //     // } else {
  //     //   setState(() => this.barcode = 'Unknown error: $e');
  //     // }
  //   } on FormatException{
  //     print('Back?');
  //   } catch (e) {
  //     print('Unknown error?');
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    // Create the initial values, if needed
    TransactionStore transactionStore = Provider.of<TransactionStore>(context);
    final CategoryStore catStore = Provider.of<CategoryStore>(context);

    if (_transactionId != null && !_inited) {
      _inited = true;
      Transaction transaction =
          transactionStore.transactionById(_transactionId);
      Category initialCategory =
          catStore.categoryById(transaction.transactionCategory);

      _title = transaction.title;
      _description = transaction.description;
      _amount = transaction.amount.abs();
      _transactionTime = transaction.date;

      _transactionType = initialCategory.transactionType;
      _transactionCategory = transaction.transactionCategory;
    }

    final List<Category> categoryList =
        catStore.categoryByType(_transactionType);

    final Map<int, Widget> segments = Map<int, Widget>();
    for (int i = 0; i < transactionTypes.length; ++i) {
      segments[i] = Text(transactionTypes[i]);
    }

    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text('Add transaction'),
        leading: CupertinoButton(
            onPressed: () {
              Navigator.of(context).pop(false);
            },
            child: Text('Cancel'),
            padding: EdgeInsets.all(0)),
        trailing: CupertinoButton(
            onPressed: () {
              saveTransaction();
            },
            child: Text('Add'),
            padding: EdgeInsets.all(0)),
      ),
      child: SafeArea(
        child: Scaffold(
            backgroundColor: Colors.white,
            body: SingleChildScrollView(
                child: Container(
                    child: Form(
                        key: _formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            // Container(
                            //     child: CupertinoButton(
                            //       child: Text('Scan bill'),
                            //       onPressed: scan,
                            //     ),
                            //     padding: EdgeInsets.fromLTRB(10, 10, 10, 20)),
                            Container(
                                child: TextFormField(
                                  initialValue: _title,
                                  validator: (value) {
                                    if (value.length < 3) {
                                      return 'Title too short';
                                    } else if (value.length > 15) {
                                      return 'Title too long';
                                    }
                                    return null;
                                  },
                                  decoration: const InputDecoration(
                                      hintText:
                                          'How should we call this transaction?',
                                      labelText: 'Title *'),
                                  onSaved: (value) {
                                    setState(() {
                                      _title = value;
                                    });
                                  },
                                ),
                                padding: EdgeInsets.fromLTRB(20, 10, 20, 20)),
                            Container(
                                child: FormField(builder: (state) {
                                  return TextFormField(
                                    initialValue: _description,
                                    validator: (value) {
                                      if (value.length > 0 &&
                                          value.length < 3) {
                                        return 'Description too short';
                                      } else if (value.length > 25) {
                                        return 'Description too long';
                                      }
                                      return null;
                                    },
                                    decoration: const InputDecoration(
                                        hintText:
                                            'Short description for the transaction',
                                        labelText: 'Description'),
                                    onSaved: (value) {
                                      setState(() {
                                        _description = value;
                                      });
                                    },
                                  );
                                }),
                                padding: EdgeInsets.fromLTRB(20, 10, 20, 20)),
                            Container(
                                child: FormField(builder: (state) {
                                  return TextFormField(
                                    initialValue: '$_amount',
                                    keyboardType: TextInputType.number,
                                    inputFormatters: <TextInputFormatter>[
                                      WhitelistingTextInputFormatter.digitsOnly
                                    ],
                                    validator: (value) {
                                      int intVal = int.tryParse(value);
                                      if (intVal == null || intVal == 0) {
                                        return 'Please enter a non-zero amount';
                                      }
                                      return null;
                                    },
                                    decoration: const InputDecoration(
                                        hintText: 'Transaction amount',
                                        labelText: 'Amount'),
                                    onSaved: (value) {
                                      setState(() {
                                        _amount = int.tryParse(value);
                                      });
                                    },
                                  );
                                }),
                                padding: EdgeInsets.fromLTRB(20, 10, 20, 20)),
                            Container(
                                height: 100,
                                child: FormField(builder: (state) {
                                  return CupertinoDatePicker(
                                      onDateTimeChanged: (dateTime) {
                                        setState(() {
                                          _transactionTime = dateTime;
                                        });
                                      },
                                      initialDateTime: DateTime.now(),
                                      mode: CupertinoDatePickerMode.date);
                                }),
                                padding: EdgeInsets.fromLTRB(20, 10, 20, 20)),
                            Container(
                              child: Text('Transaction type'),
                              padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
                            ),
                            FractionallySizedBox(
                                widthFactor: 1,
                                child: CupertinoSegmentedControl<int>(
                                  children: segments,
                                  groupValue: _transactionType,
                                  onValueChanged: (value) {
                                    setState(() {
                                      _transactionType = value;
                                    });
                                  },
                                )),
                            Container(
                                child: Column(
                              children: List<Widget>.generate(
                                  categoryList.length, (idx) {
                                return Row(children: <Widget>[
                                  Container(
                                      width: 4,
                                      margin: EdgeInsets.all(4),
                                      height: 60,
                                      decoration:
                                          BoxDecoration(color: categoryList[idx].color)),
                                  Expanded(
                                      child: RadioListTile(
                                    title: Text('${categoryList[idx].title}'),
                                    subtitle: Text(
                                        '${categoryList[idx].description}'),
                                    groupValue: _transactionCategory,
                                    value: categoryList[idx].id,
                                    onChanged: setTransactionCategory,
                                  )),
                                ]);
                              }),
                            ))
                          ],
                        ))))),
      ),
    );
  }
}
