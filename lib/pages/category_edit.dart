import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_material_color_picker/flutter_material_color_picker.dart';
import 'package:my_money_tracker/models/categories.dart';
import 'package:provider/provider.dart';

class CategoryEditPage extends StatefulWidget {
  final int categoryId;
  CategoryEditPage({Key key, this.categoryId}) : super(key: key);

  @override
  _CategoryEditPageState createState() => _CategoryEditPageState();
}

const double _kPickerSheetHeight = 80;
const double _kPickerItemHeight = 32;

class _CategoryEditPageState extends State<CategoryEditPage> {
  final _formKey = GlobalKey<FormState>();

  bool _inited;
  int _categoryId;
  
  String _title;
  String _description;
  int _transactionType = 0;

  Color _tempShadeColor;
  Color _shadeColor = Colors.blue[800];

  @override
  void initState() {
    super.initState();

    _categoryId = widget.categoryId;
    _inited = false;
  }

  void saveCategory() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      Navigator.of(context).pop(false);
      CategoryStore catStore = Provider.of<CategoryStore>(context);

      if (_categoryId == null) {
        int id;
        do {
          id = Random.secure().nextInt(1 << 32);
        } while (catStore.categoryById(id) != null);
        catStore.addCategory(Category(
            id: id,
            title: _title,
            description: _description,
            transactionType: _transactionType,
            color: _shadeColor));
      } else {
        catStore.updateCategory(Category(
            id: _categoryId,
            title: _title,
            description: _description,
            transactionType: _transactionType,
            color: _shadeColor));
      }
    }
  }

  void _openDialog(String title, Widget content, BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.all(6.0),
          title: Text(title),
          content: SizedBox(
              width: double.maxFinite,
              height: double.maxFinite,
              child: content),
          actions: [
            CupertinoButton(
              child: Text('Cancel'),
              onPressed: Navigator.of(context).pop,
            ),
            CupertinoButton(
              child: Text('Submit'),
              onPressed: () {
                Navigator.of(context).pop();
                // setState(() => _mainColor = _tempMainColor);
                setState(() => _shadeColor = _tempShadeColor);
              },
            ),
          ],
        );
      },
    );
  }

  void _openColorPicker(BuildContext context) async {
    _openDialog(
        "Color picker",
        MaterialColorPicker(
          selectedColor: _shadeColor,
          onColorChange: (color) => setState(() => _tempShadeColor = color),
          onBack: () => print("Back button pressed"),
        ),
        context);
  }

  @override
  Widget build(BuildContext context) {
    // Create the initial values, if needed
    if (_categoryId != null && !_inited) {
      _inited = true;
      CategoryStore catStore = Provider.of<CategoryStore>(context);
      Category category = catStore.categoryById(_categoryId);

      _title = category.title;
      _description = category.description;
      _transactionType = category.transactionType;
      _shadeColor = category.color;
    }

    final FixedExtentScrollController scrollController =
        FixedExtentScrollController(initialItem: _transactionType);

    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text('Add category'),
        leading: CupertinoButton(
            onPressed: () {
              Navigator.of(context).pop(false);
            },
            child: Text('Cancel'),
            padding: EdgeInsets.all(0)),
        trailing: CupertinoButton(
            onPressed: () {
              saveCategory();
            },
            child: Text('Save'),
            padding: EdgeInsets.all(0)),
      ),
      child: SafeArea(
        child: Scaffold(
            backgroundColor: Colors.white,
            body: SingleChildScrollView(
              child: Container(
                  padding: EdgeInsets.all(20),
                  child: Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              child: TextFormField(
                                initialValue: _title,
                                validator: (value) {
                                  if (value.length < 3) {
                                    return 'Title too short';
                                  } else if (value.length > 15) {
                                    return 'Title too long';
                                  }
                                  return null;
                                },
                                decoration: const InputDecoration(
                                    hintText:
                                        'How should we call this category?',
                                    labelText: 'Title *'),
                                onSaved: (value) {
                                  setState(() {
                                    _title = value;
                                  });
                                },
                              ),
                              padding: EdgeInsets.fromLTRB(0, 10, 0, 20)),
                          Container(
                              child: FormField(builder: (state) {
                                return TextFormField(
                                  initialValue: _description,
                                  validator: (value) {
                                    if (value.length > 0 && value.length < 3) {
                                      return 'Description too short';
                                    } else if (value.length > 15) {
                                      return 'Description too long';
                                    }
                                    return null;
                                  },
                                  decoration: const InputDecoration(
                                      hintText:
                                          'Short description for the category',
                                      labelText: 'Description'),
                                  onSaved: (value) {
                                    setState(() {
                                      _description = value;
                                    });
                                  },
                                );
                              }),
                              padding: EdgeInsets.fromLTRB(0, 10, 0, 20)),
                          Container(
                              padding: EdgeInsets.fromLTRB(0, 20, 0, 10),
                              child: GestureDetector(
                                onTap: () {
                                  _openColorPicker(context);
                                },
                                child: Row(
                                  children: <Widget>[
                                    CupertinoButton(
                                      child: Text('Choose color'),
                                      onPressed: () {
                                        _openColorPicker(context);
                                      },
                                    ),
                                    CircleColor(
                                      color: _shadeColor,
                                      circleSize: 30,
                                    )
                                  ],
                                ),
                              )),
                          Container(
                            child: Text('Category type:'),
                            padding: EdgeInsets.fromLTRB(0, 20, 0, 10),
                          ),
                          Container(
                            height: _kPickerSheetHeight,
                            child: CupertinoPicker(
                                scrollController: scrollController,
                                onSelectedItemChanged: (int idx) {
                                  _transactionType = idx;
                                },
                                children: List<Widget>.generate(
                                    transactionTypes.length, (idx) {
                                  return Center(
                                      child: Text('${transactionTypes[idx]}'));
                                }),
                                itemExtent: _kPickerItemHeight,
                                backgroundColor: CupertinoColors.white),
                          )
                        ],
                      ))),
            )),
      ),
    );
  }
}
