import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:my_money_tracker/components/dialogs/recategorize_dialog.dart';
import 'package:my_money_tracker/models/transactions.dart';
import 'package:my_money_tracker/pages/category_edit.dart';
import 'package:provider/provider.dart';
import '../models/categories.dart';

class CategoriesPage extends StatefulWidget {
  CategoriesPage({Key key}) : super(key: key);

  @override
  _CategoriesPageState createState() => _CategoriesPageState();
}

class _CategoriesPageState extends State<CategoriesPage> {
  _deleteDialog(BuildContext ctx, int categoryId) {
    final categoryStore = Provider.of<CategoryStore>(context);
    final transactionStore = Provider.of<TransactionStore>(context);
    showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            actions: <Widget>[
              CupertinoButton(
                child: Text('No'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              CupertinoButton(
                child: Text('Yes'),
                onPressed: () {
                  Navigator.of(context).pop();
                  List<Transaction> transactions =
                      transactionStore.transactionsInCategory(categoryId);
                  print(transactions);
                  if (transactions.length > 0) {
                    _recategorizeAlert(ctx, categoryId);
                  } else {
                    categoryStore.removeCategory(categoryId);
                  }
                },
              ),
            ],
            title: Text('Are you sure?'),
            content: Text('Are you sure you want to delete this category?'),
          );
        });
  }

  _recategorizeAlert(BuildContext context, int categoryId) {
    final categoryStore = Provider.of<CategoryStore>(context);
    final transactionStore = Provider.of<TransactionStore>(context);
    int targetCategory;

    showCupertinoDialog(
        context: context,
        builder: (context) {
          return RecategorizeDialog(
            currentCategoryId: categoryId,
            catStore: categoryStore,
            onCategoryChanged: (catId) {
              targetCategory = catId;
            },
            onSubmit: () {
              transactionStore.recategorizeTransactions(
                  categoryId, targetCategory);
              categoryStore.removeCategory(categoryId);
            },
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    final categoryStore = Provider.of<CategoryStore>(context);
    final SlidableController slidableController = SlidableController();

    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text('Categories'),
        trailing: GestureDetector(
            onTap: () {
              Navigator.push(context,
                  CupertinoPageRoute(builder: (context) => CategoryEditPage()));
            },
            child: Icon(CupertinoIcons.add)),
      ),
      child: SafeArea(
        child: Scaffold(
          body: ListView(
            children: categoryStore.categories.map((category) {
              return Slidable(
                controller: slidableController,
                actionPane: SlidableBehindActionPane(),
                actionExtentRatio: 0.25,
                child: Container(
                    color: Colors.white,
                    child: Row(children: <Widget>[
                      Container(
                          width: 4,
                          margin: EdgeInsets.all(4),
                          height: 60,
                          decoration: BoxDecoration(color: category.color)),
                      Expanded(
                          child: ListTile(
                        title: Text('${category.title}'),
                        subtitle: Text('${category.description}'),
                      )),
                    ])),
                secondaryActions: <Widget>[
                  IconSlideAction(
                    caption: 'Edit',
                    color: Colors.blue,
                    icon: CupertinoIcons.pen,
                    onTap: () {
                      Navigator.push(
                          context,
                          CupertinoPageRoute(
                              builder: (context) => CategoryEditPage(
                                    categoryId: category.id,
                                  )));
                    },
                  ),
                  IconSlideAction(
                    caption: 'Delete',
                    color: Colors.red,
                    icon: CupertinoIcons.delete,
                    onTap: () {
                      _deleteDialog(context, category.id);
                    },
                  ),
                ],
              );
            }).toList(),
          ),
        ),
      ),
    );
  }
}
