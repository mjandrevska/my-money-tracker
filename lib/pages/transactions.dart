import 'dart:convert';
import 'dart:io';

import 'package:csv/csv.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:intl/intl.dart';
import 'package:my_money_tracker/models/transactions.dart';
import 'package:my_money_tracker/models/categories.dart';
import 'package:my_money_tracker/pages/transaction_edit.dart';
import 'package:provider/provider.dart';
import 'package:path_provider/path_provider.dart';

class TransactionsPage extends StatefulWidget {
  TransactionsPage({Key key}) : super(key: key);

  @override
  _TransactionsPageState createState() => _TransactionsPageState();
}

class _TransactionsPageState extends State<TransactionsPage> {
  _deleteDialog(BuildContext context, int transactionId) {
    final transactionStore = Provider.of<TransactionStore>(context);
    showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            actions: <Widget>[
              CupertinoButton(
                child: Text('Yes'),
                onPressed: () {
                  Navigator.of(context).pop();
                  transactionStore.removeTransaction(transactionId);
                },
              ),
              CupertinoButton(
                child: Text('No'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
            title: Text('Are you sure?'),
            content: Text('Are you sure you want to delete this transaction?'),
          );
        });
  }

  _saveCsv(String csvData) async {
    List<int> bytes = utf8.encode(csvData);
    Share.file('transactions', 'transactions.csv', bytes, 'text/csv');
  }

  @override
  Widget build(BuildContext context) {
    final transactionStore = Provider.of<TransactionStore>(context);
    final catStore = Provider.of<CategoryStore>(context);
    final DateFormat dateFormatter = new DateFormat('dd.MM.yyyy');
    final SlidableController slidableController = SlidableController();

    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
          middle: Text('Transactions'),
          trailing: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
            GestureDetector(
                onTap: () {
                  List<List<dynamic>> csvData = new List();
                  for(int i=0; i<transactionStore.transactions.length; ++i) {
                    List<dynamic> row = new List();
                    Transaction trans = transactionStore.transactions[i];
                    Category cat = catStore.categoryById(transactionStore.transactions[i].transactionCategory);
                    row.addAll([trans.id, trans.title, trans.description, trans.amount, trans.date, trans.transactionCategory, cat.title, cat.description, cat.transactionType, cat.color]);
                    csvData.add(row);
                  }

                  String csv = const ListToCsvConverter().convert(csvData);
                  _saveCsv(csv);
                },
                child: Icon(CupertinoIcons.share)),
            GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      CupertinoPageRoute(
                          builder: (context) => TransactionEditPage()));
                },
                child: Icon(CupertinoIcons.add))
          ])),
      child: SafeArea(
        child: Scaffold(
          body: ListView.builder(
              itemCount: transactionStore.transactions.length,
              itemBuilder: (BuildContext context, int index) {
                final Transaction transaction =
                    transactionStore.transactions[index];
                final Category category =
                    catStore.categoryById(transaction.transactionCategory);
                final Color amountColor =
                    transaction.amount > 0 ? Colors.green : Colors.red;
                return Slidable(
                  controller: slidableController,
                  actionPane: SlidableBehindActionPane(),
                  actionExtentRatio: 0.25,
                  child: Container(
                      color: Colors.white,
                      child: ListTile(
                        key: Key('${transaction.id}'),
                        title: Text('${transaction.title}'),
                        subtitle: Text(
                            '${transaction.description}\n${dateFormatter.format(transaction.date)}'),
                        trailing: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                '${transaction.amount}',
                                style:
                                    TextStyle(color: amountColor, fontSize: 18),
                                textAlign: TextAlign.center,
                              ),
                              Text(
                                '${category.title}',
                                style: TextStyle(
                                    color: category.color, fontSize: 12),
                                textAlign: TextAlign.center,
                              )
                            ]),
                        isThreeLine: true,
                      )),
                  secondaryActions: <Widget>[
                    IconSlideAction(
                      caption: 'Edit',
                      color: Colors.blue,
                      icon: CupertinoIcons.pen,
                      onTap: () {
                        Navigator.push(
                            context,
                            CupertinoPageRoute(
                                builder: (context) => TransactionEditPage(
                                      transactionId: transaction.id,
                                    )));
                      },
                    ),
                    IconSlideAction(
                      caption: 'Delete',
                      color: Colors.red,
                      icon: CupertinoIcons.delete,
                      onTap: () {
                        _deleteDialog(context, transaction.id);
                      },
                    )
                  ],
                );
              }),
        ),
      ),
    );
  }
}
