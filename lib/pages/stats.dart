import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_money_tracker/components/charts/CategoriesChart.dart';
import 'package:my_money_tracker/components/charts/TransactionsChart.dart';
import 'package:my_money_tracker/models/categories.dart';

const List<String> chartTypes = <String>['Transactions', 'Categories'];

class StatsPage extends StatefulWidget {
  StatsPage({Key key}) : super(key: key);

  @override
  _StatsPageState createState() => _StatsPageState();
}

class _StatsPageState extends State<StatsPage> {
  int _currentChartType = 0;
  int _currentTransactionType = 0;

  @override
  Widget build(BuildContext context) {
    final Map<int, Widget> segments = Map<int, Widget>();
    for (int i = 0; i < chartTypes.length; ++i) {
      segments[i] = Text(chartTypes[i]);
    }

    var chartWidget;

    if (_currentChartType == 0) {
      chartWidget = TransactionsChart.withProviderData(context);
    } else {
      final Map<int, Widget> transTypeSegments = Map<int, Widget>();
      for (int i = 0; i < transactionTypes.length; ++i) {
        transTypeSegments[i] = Text(transactionTypes[i]);
      }
      chartWidget = Column(children: <Widget>[
        Expanded(
            child: CategoriesChart.withProviderData(
                context, _currentTransactionType)),
        FractionallySizedBox(
            widthFactor: 1,
            child: CupertinoSegmentedControl(
              children: transTypeSegments,
              groupValue: _currentTransactionType,
              onValueChanged: (value) {
                setState(() {
                  _currentTransactionType = value;
                });
              },
            )),
      ]);
    }

    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text('Stats'),
      ),
      child: SafeArea(
        child: Column(children: <Widget>[
          Container(
            padding: EdgeInsets.all(10),
            child: FractionallySizedBox(
                widthFactor: 1,
                child: CupertinoSegmentedControl(
                  children: segments,
                  groupValue: _currentChartType,
                  onValueChanged: (value) {
                    setState(() {
                      _currentChartType = value;
                    });
                  },
                )),
          ),
          Expanded(
              child: Container(padding: EdgeInsets.all(20), child: chartWidget))
        ]),
      ),
    );
  }
}
