import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_money_tracker/models/categories.dart';

class RecategorizeDialog extends StatefulWidget {
  final int currentCategoryId;
  final CategoryStore catStore;
  final Function onSubmit;
  final Function(int categoryId) onCategoryChanged;
  RecategorizeDialog(
      {key,
      this.catStore,
      this.currentCategoryId,
      this.onSubmit,
      this.onCategoryChanged})
      : super(key: key);

  @override
  _RecategorizeDialogState createState() => _RecategorizeDialogState();
}

class _RecategorizeDialogState extends State<RecategorizeDialog> {
  bool inited;

  int _newCategoryType;
  int _newCategoryId;

  @override
  void initState() {
    super.initState();
    inited = false;
  }

  @override
  Widget build(BuildContext context) {
    if (!inited) {
      _newCategoryType = 0;
    }
    final List<Category> categoryList =
        widget.catStore.categoryByType(_newCategoryType);
    categoryList.removeWhere((cat) => cat.id == widget.currentCategoryId);
    if (!inited) {
      inited = true;
      _newCategoryId = categoryList.length > 0 ? categoryList.first.id : null;
      widget.onCategoryChanged(_newCategoryId);
    }

    final Map<int, Widget> segments = Map<int, Widget>();
    for (int i = 0; i < transactionTypes.length; ++i) {
      segments[i] = Text(transactionTypes[i]);
    }

    return CupertinoAlertDialog(
        actions: <Widget>[
          CupertinoButton(
            child: Text('Cancel'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          CupertinoButton(
            child: Text('Confirm'),
            onPressed: _newCategoryId == null
                ? null
                : () {
                    Navigator.of(context).pop();
                    widget.onSubmit();
                  },
          ),
        ],
        title: Text('Recategorize'),
        content: Material(
            color: CupertinoColors.white,
            child: Column(children: <Widget>[
              Text(
                  'There are transactions under this category. Choose which category to put them under.'),
              FractionallySizedBox(
                  widthFactor: 1,
                  child: CupertinoSegmentedControl<int>(
                    children: segments,
                    groupValue: _newCategoryType,
                    onValueChanged: (value) {
                      setState(() {
                        _newCategoryType = value;
                      });
                    },
                    padding: EdgeInsets.fromLTRB(2, 20, 2, 10),
                  )),
              Container(
                  child: Column(
                children: List<Widget>.generate(categoryList.length, (idx) {
                  return Row(children: <Widget>[
                    Container(
                        width: 4,
                        margin: EdgeInsets.all(4),
                        height: 60,
                        decoration:
                            BoxDecoration(color: categoryList[idx].color)),
                    Expanded(
                        child: RadioListTile(
                      title: Text('${categoryList[idx].title}'),
                      subtitle: Text('${categoryList[idx].description}'),
                      groupValue: _newCategoryId,
                      value: categoryList[idx].id,
                      onChanged: (value) {
                        setState(() {
                          _newCategoryId = value;
                        });
                        widget.onCategoryChanged(value);
                      },
                    )),
                  ]);
                }),
              ))
            ])));
  }
}
