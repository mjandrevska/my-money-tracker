import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:my_money_tracker/models/categories.dart';
import 'package:my_money_tracker/models/transactions.dart';
import 'package:provider/provider.dart';

class CategoriesChart extends StatelessWidget {
  final List<charts.Series> seriesList;

  CategoriesChart(this.seriesList);

  /// Creates a [PieChart] with sample data and no transition.
  factory CategoriesChart.withProviderData(
      BuildContext context, int transactionType) {
    TransactionStore transStore = Provider.of<TransactionStore>(context);
    CategoryStore catStore = Provider.of<CategoryStore>(context);

    List<charts.Series> series = new List();

    // Get cats by type
    List<Category> categoriesByType = catStore.categoryByType(transactionType);

    series.add(new charts.Series<Category, int>(
        id: transactionTypes[transactionType],
        domainFn: (Category cat, _) => cat.id,
        measureFn: (Category cat, _) {
          int sum = 0;
          transStore.transactionsInCategory(cat.id).forEach((trans) {
            sum += trans.amount.abs();
          });
          return sum;
        },
        colorFn: (Category cat, _) => charts.Color(
            r: cat.color.red,
            g: cat.color.green,
            b: cat.color.blue,
            a: cat.color.alpha),
        data: categoriesByType,
        labelAccessorFn: (Category cat, _) {
          int sum = 0;
          transStore.transactionsInCategory(cat.id).forEach((trans) {
            sum += trans.amount;
          });
          return '${cat.title}: $sum';
        }));

    return new CategoriesChart(
      series,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new charts.PieChart(seriesList,
        // Configure the width of the pie slices to 60px. The remaining space in
        // the chart will be left as a hole in the center.
        //
        // [ArcLabelDecorator] will automatically position the label inside the
        // arc if the label will fit. If the label will not fit, it will draw
        // outside of the arc with a leader line. Labels can always display
        // inside or outside using [LabelPosition].
        //
        // Text style for inside / outside can be controlled independently by
        // setting [insideLabelStyleSpec] and [outsideLabelStyleSpec].
        //
        // Example configuring different styles for inside/outside:
        //       new charts.ArcLabelDecorator(
        //          insideLabelStyleSpec: new charts.TextStyleSpec(...),
        //          outsideLabelStyleSpec: new charts.TextStyleSpec(...)),
        defaultRenderer: new charts.ArcRendererConfig(
            arcWidth: 60,
            arcRendererDecorators: [new charts.ArcLabelDecorator()]));
  }

  /// Create one series with sample hard coded data.
  // static List<charts.Series<LinearSales, int>> _createSampleData() {
  //   final data = [
  //     new LinearSales(0, 100),
  //     new LinearSales(1, 75),
  //     new LinearSales(2, 25),
  //     new LinearSales(3, 5),
  //   ];

  //   return [
  //     new charts.Series<LinearSales, int>(
  //       id: 'Sales',
  //       domainFn: (LinearSales sales, _) => sales.year,
  //       measureFn: (LinearSales sales, _) => sales.sales,
  //       colorFn: ,
  //       data: data,
  //       // Set a label accessor to control the text of the arc label.
  //       labelAccessorFn: (LinearSales row, _) => '${row.year}: ${row.sales}',
  //     )
  //   ];
  // }
}

/// Sample linear data type.
class LinearSales {
  final int year;
  final int sales;

  LinearSales(this.year, this.sales);
}
