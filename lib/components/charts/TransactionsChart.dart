import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:my_money_tracker/models/categories.dart';
import 'package:my_money_tracker/models/transactions.dart';
import 'package:provider/provider.dart';

class TransactionsChart extends StatelessWidget {
  final List<charts.Series> seriesList;

  TransactionsChart(this.seriesList);

  /// Creates a [TimeSeriesChart] with sample data and no transition.
  factory TransactionsChart.withProviderData(BuildContext context) {
    TransactionStore transStore = Provider.of<TransactionStore>(context);
    CategoryStore catStore = Provider.of<CategoryStore>(context);

    List<Transaction> incomes = transStore.transactions.where((transaction) {
      final catIdx =
          catStore.categoriesIdxById[transaction.transactionCategory];
      return catStore.categories[catIdx].transactionType == 0;
    }).toList();
    List<Transaction> expenses = transStore.transactions.where((transaction) {
      final catIdx =
          catStore.categoriesIdxById[transaction.transactionCategory];
      return catStore.categories[catIdx].transactionType == 1;
    }).toList();

    List<charts.Series<Transaction, DateTime>> series = new List();
    series.add(new charts.Series<Transaction, DateTime>(
      id: 'Incomes',
      colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
      domainFn: (Transaction transaction, _) => transaction.date,
      measureFn: (Transaction transaction, _) => transaction.amount,
      data: incomes,
    ));
    series.add(new charts.Series<Transaction, DateTime>(
      id: 'Expenses',
      colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
      domainFn: (Transaction transaction, _) => transaction.date,
      measureFn: (Transaction transaction, _) => transaction.amount,
      data: expenses,
    ));

    return new TransactionsChart(
      series,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new charts.TimeSeriesChart(
      seriesList,
      // Configure the default renderer as a line renderer. This will be used
      // for any series that does not define a rendererIdKey.
      //
      // This is the default configuration, but is shown here for  illustration.
      defaultRenderer: new charts.LineRendererConfig(),
      // Optionally pass in a [DateTimeFactory] used by the chart. The factory
      // should create the same type of [DateTime] as the data provided. If none
      // specified, the default creates local date time.
      dateTimeFactory: const charts.LocalDateTimeFactory(),
    );
  }
}
